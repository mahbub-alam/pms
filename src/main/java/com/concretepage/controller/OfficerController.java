package com.concretepage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.concretepage.entity.Article;
import com.concretepage.entity.Officer;
import com.concretepage.service.IOfficerService;
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("officer")
public class OfficerController {
	@Autowired
	private IOfficerService officerService;
	@GetMapping("getOfficer/{id}")
	public ResponseEntity<Officer> getOfficerById(@PathVariable("id") Integer id) {
		Officer officer = officerService.getOfficerById(id);
		return new ResponseEntity<Officer>(officer, HttpStatus.OK);
	}
	
	@GetMapping("officers")
	public ResponseEntity<List<Officer>> getAllOfficers(){
		List<Officer> officers = officerService.getAllOfficers();
		return new ResponseEntity<List<Officer>>(officers, HttpStatus.OK);
	}
	@PostMapping("addOfficer")
	public ResponseEntity<List<Officer>> addOfficer(@RequestBody Officer officer, UriComponentsBuilder builder) {
		officerService.addOfficer(officer);
		List<Officer> officers = officerService.getAllOfficers();
		return new ResponseEntity<List<Officer>>(officers, HttpStatus.OK);
	}
	@PutMapping("updateOfficer")
	public ResponseEntity<List<Officer>> updateOfficer(@RequestBody Officer officer, UriComponentsBuilder builder){
		officerService.updateOfficer(officer);
		List<Officer> officers = officerService.getAllOfficers();
		return new ResponseEntity<List<Officer>>(officers, HttpStatus.OK);
	}
	
	@DeleteMapping("deleteOfficer/{id}")
	public ResponseEntity<List<Officer>> deleteOfficer(@PathVariable("id") Integer id){
		officerService.deleteOfficer(id);
		List<Officer> officers = officerService.getAllOfficers();
		return new ResponseEntity<List<Officer>>(officers, HttpStatus.OK);
	}
	@GetMapping("searchOfficer/{searchKey}")
	public ResponseEntity<List<Officer>> searchOfficers(@PathVariable("searchKey") String searchKey){
		List<Officer> officers = officerService.searchOfficers(searchKey);
		return new ResponseEntity<List<Officer>>(officers, HttpStatus.OK);
	}
}
