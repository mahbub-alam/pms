package com.concretepage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.concretepage.entity.User;
import com.concretepage.service.IUserService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("user")
@ComponentScan("com.concretepage")
public class UserController {
	@Autowired
	private IUserService userService;
	@GetMapping("login/{email}/{password}")
	public ResponseEntity<User> getUserByIdPass(@PathVariable("email") String email,@PathVariable("password") String password){
		System.out.println(email);
		System.out.println(password);
		User user = new User();
	    user = userService.getUserByIdPass(email, password);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
}
