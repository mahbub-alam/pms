package com.concretepage.dao;

import java.util.List;

import com.concretepage.entity.Officer;

public interface IOfficerDAO {
	List<Officer> getAllOfficers();
	Officer getOfficerById(int officerId);
	void addOfficer(Officer officer);
	void updateOfficer(Officer officer);
	void deleteOfficer(int officerId);
	List<Officer> searchOfficers(String searchKey);
}
