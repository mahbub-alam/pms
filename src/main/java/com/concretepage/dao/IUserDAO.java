package com.concretepage.dao;

import com.concretepage.entity.User;

public interface IUserDAO {

	User getUserByIdPass(String email, String password);

}
