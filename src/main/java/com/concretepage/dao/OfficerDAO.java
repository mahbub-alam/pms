package com.concretepage.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.concretepage.entity.Officer;
import com.concretepage.entity.OfficerRowMapper;
@Transactional
@Repository
public class OfficerDAO implements IOfficerDAO{
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	//All Officer List
	public List<Officer> getAllOfficers() {
		String sql = "SELECT * FROM officer";
		RowMapper<Officer> rowMapper = new OfficerRowMapper();
		return this.jdbcTemplate.query(sql, rowMapper);
	}
	
	//Get Officer By Id
	@Override
	public Officer getOfficerById(int officerId) {
		String sql = "SELECT * FROM officer WHERE officerId = ?";
		RowMapper<Officer> rowMapper = new BeanPropertyRowMapper<Officer>(Officer.class);
		Officer officer = jdbcTemplate.queryForObject(sql, rowMapper, officerId);
		return officer;
	}
	//Update Officer By Id

	@Override
	public void addOfficer(Officer officer) {
		//Add article
				String sql = "INSERT INTO officer (birth_place, blood_group, date_of_birth, email, "
						+ "height, image_path, mobile, name, nationality, nid_no, passport_no, "
						+ "personal_no, permanent_address, present_address, present_medical_category, "
						+ "rank, religion, service, village, visible_Identification_mark, weight) "
						+ "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				jdbcTemplate.update(sql, officer.getBirth_place(), officer.getBlood_group(), officer.getDate_of_birth(), officer.getEmail(), 
						officer.getHeight(), officer.getImage_path(), officer.getMobile(), officer.getName(), officer.getNationality(), officer.getNid_no(), officer.getPassport_no(), 
						officer.getPersonal_no(), officer.getPermanent_address(), officer.getPresent_address(), officer.getPresent_medical_category(), 
						officer.getRank(), officer.getReligion(), officer.getService(), officer.getVillage(), officer.getVisible_Identification_mark(), officer.getWeight());
		
	}
	public void updateOfficer(Officer officer) {
				String sql = "UPDATE officer SET birth_place=?, blood_group=?, date_of_birth=?, email=?," 
						+"height=?, image_path=?, mobile=?, name=?, nationality=?, nid_no=?, passport_no=?," 
						+"personal_no=?, permanent_address=?, present_address=?, present_medical_category=?," 
						+"rank=?, religion=?, service=?, village=?, visible_Identification_mark=?, weight=? WHERE officerId=?";
				jdbcTemplate.update(sql,officer.getBirth_place(), officer.getBlood_group(), officer.getDate_of_birth(), officer.getEmail(),
						officer.getHeight(), officer.getImage_path(), officer.getMobile(), officer.getName(), officer.getNationality(), officer.getNid_no(), officer.getPassport_no(),
						officer.getPersonal_no(), officer.getPermanent_address(), officer.getPresent_address(), officer.getPresent_medical_category(),
						officer.getRank(), officer.getReligion(), officer.getService(), officer.getVillage(), officer.getVisible_Identification_mark(), officer.getWeight(),officer.getOfficerId()
						);
	}
	
	public void deleteOfficer(int officerId) {
		String sql = "DELETE officer from officer where officerId=?";
		jdbcTemplate.update(sql,officerId);
	}
	
	public List<Officer> searchOfficers(String searchKey){
		List<Officer> officerList = new ArrayList<>();
		Object[] parameters = new Object[] { "%"+searchKey+"%"};
		String sql;
		RowMapper<Officer> rowMapper = new OfficerRowMapper();
		System.out.println(searchKey+"*********");
		if(searchKey.equals("notfound")) {
			String sql1 = "SELECT * FROM officer";
			System.out.println(sql1);
			RowMapper<Officer> rowMapper1 = new OfficerRowMapper();
			return this.jdbcTemplate.query(sql1, rowMapper1);
		}
		else {
			sql = "SELECT * from officer where name LIKE ?";
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("name", searchKey+"%");
			List<Map<String, Object>> rows = this.jdbcTemplate.queryForList(sql, parameters);
			  for (Map row : rows) {
				  Officer officer = new Officer();
				  	officer.setOfficerId((int)row.get("officerId"));
					officer.setBirth_place((String)row.get("birth_place"));
					officer.setBlood_group((String)row.get("blood_group"));
					officer.setDate_of_birth((String)row.get("date_of_birth"));
					officer.setEmail((String)row.get("email"));
					officer.setHeight((String)row.get("height"));
					officer.setImage_path((String)row.get("image_path"));
					officer.setMobile((String)row.get("mobile"));
					officer.setName((String)row.get("name"));
					officer.setNationality((String)row.get("nationality"));
					officer.setNid_no((String)row.get("nid_no"));
					officer.setPassport_no((String)row.get("passport_no"));
					officer.setPermanent_address((String)row.get("permanent_address"));
					officer.setPersonal_no((String)row.get("personal_no"));
					officer.setPresent_address((String)row.get("present_address"));
					officer.setPresent_medical_category((String)row.get("present_medical_category"));
					officer.setRank((String)row.get("rank"));
					officer.setReligion((String)row.get("religion"));
					officer.setService((String)row.get("service"));
					officer.setVillage((String)row.get("village"));
					officer.setVisible_Identification_mark((String)row.get("visible"));
					officer.setWeight((String)row.get("weight"));
				  officerList.add(officer);
			    }
			return officerList;
		}
		
		}
	
}
