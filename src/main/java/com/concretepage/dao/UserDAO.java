package com.concretepage.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.concretepage.entity.User;
import com.concretepage.entity.UserRowMapper;
@Transactional
@Repository
public class UserDAO implements IUserDAO{
	@Autowired
    private JdbcTemplate jdbcTemplate;
	@Override
	public User getUserByIdPass(String email,String password) {
		//String sql = "SELECT * FROM user WHERE email = ? AND password = ?";
		RowMapper<User> rowMapper = new UserRowMapper();
		String sql = "SELECT * FROM user WHERE email = ?";
		//User user = jdbcTemplate.query(sql, rowMapper, email);
		System.out.println("*****************************************************");
		return this.jdbcTemplate.queryForObject(sql, rowMapper, email);
		//return user;
	}
}
