package com.concretepage.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.concretepage.entity.Officer;
public class OfficerRowMapper implements RowMapper<Officer>{

	@Override
	public Officer mapRow(ResultSet rs, int rowNum) throws SQLException {
		Officer officer = new Officer();
		officer.setOfficerId(rs.getInt("officerId"));
		officer.setBirth_place(rs.getString("birth_place"));
		officer.setBlood_group(rs.getString("blood_group"));
		officer.setDate_of_birth(rs.getString("date_of_birth"));
		officer.setEmail(rs.getString("email"));
		officer.setHeight(rs.getString("height"));
		officer.setImage_path(rs.getString("image_path"));
		officer.setMobile(rs.getString("mobile"));
		officer.setName(rs.getString("name"));
		officer.setNationality(rs.getString("nationality"));
		officer.setNid_no(rs.getString("nid_no"));
		officer.setPassport_no(rs.getString("passport_no"));
		officer.setPermanent_address(rs.getString("permanent_address"));
		officer.setPersonal_no(rs.getString("personal_no"));
		officer.setPresent_address(rs.getString("present_address"));
		officer.setPresent_medical_category(rs.getString("present_medical_category"));
		officer.setRank(rs.getString("rank"));
		officer.setReligion(rs.getString("religion"));
		officer.setService(rs.getString("service"));
		officer.setVillage(rs.getString("village"));
		officer.setVisible_Identification_mark(rs.getString("visible_Identification_mark"));
		officer.setWeight(rs.getString("weight"));
		return officer;
	}

}