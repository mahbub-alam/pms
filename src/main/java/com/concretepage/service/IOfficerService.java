package com.concretepage.service;

import java.util.List;
import com.concretepage.entity.Officer;

public interface IOfficerService {
	List<Officer> getAllOfficers();
	Officer getOfficerById(int officerId);
	void addOfficer(Officer officer);
	void updateOfficer(Officer officer);
	void deleteOfficer(int officerId);
	List<Officer> searchOfficers(String searchKey);
}
