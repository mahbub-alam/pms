package com.concretepage.service;

import com.concretepage.entity.User;

public interface IUserService {

	User getUserByIdPass(String email, String password);

}
