package com.concretepage.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.concretepage.dao.IOfficerDAO;
import com.concretepage.entity.Officer;

@Service
public class OfficerService implements IOfficerService{
	@Autowired
	private IOfficerDAO officerDAO;
	public List<Officer> getAllOfficers(){
		return officerDAO.getAllOfficers();
	}
	@Override
	public Officer getOfficerById(int officerId) {
		Officer officer = officerDAO.getOfficerById(officerId);
		return officer;
	}
	@Override
	public void addOfficer(Officer officer) {
		officerDAO.addOfficer(officer);
		
	}
	@Override
	public void updateOfficer(Officer officer) {
		officerDAO.updateOfficer(officer);
	}
	@Override
	public void deleteOfficer(int officerId) {
		officerDAO.deleteOfficer(officerId);
	}
	@Override
	public List<Officer> searchOfficers(String searchKey){
		return officerDAO.searchOfficers(searchKey);
	}
	
}
