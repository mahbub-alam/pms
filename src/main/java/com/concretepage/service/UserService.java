package com.concretepage.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.concretepage.dao.IUserDAO;
import com.concretepage.entity.User;

@Service
public class UserService implements IUserService{
	@Autowired
	private IUserDAO userDAO;
	@Override
	public User getUserByIdPass(String email, String password) {
		System.out.println(email);
		System.out.println(password);
		User user = new User();
		user = userDAO.getUserByIdPass(email, password);
		return user;
	}
}
